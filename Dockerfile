FROM arm32v7/python:3-buster
WORKDIR /usr/src/app
COPY . .

RUN pip3 install requests bs4 termcolor schedule arrow
# install dependencies
CMD ["python3","-u","main.py", "/store", "--schedule"]