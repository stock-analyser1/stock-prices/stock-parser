import datetime
import data_scraper
import csv
import os
from termcolor import colored
import arrow


class DataController:

    def __init__(self):
        self.data = None
        self.storage = ""

    @property
    def storage(self):
        return self._storage

    @storage.setter
    def storage(self, value):
        self._storage = value

    def get_last_date(self, symbol):
        with open(f"{self.storage}/{symbol}_prices.csv") as f:
            data = list(csv.DictReader(f))
        return datetime.date.fromisoformat(data[-1]["Date"])

    def get_data_by_symbol(self, symbol):
        return dict(list(filter(lambda x: x['name'] == symbol, self.data))[0])

    def get_closing_price_by_symbol(self, symbol):
        return self.get_data_by_symbol(symbol)['last']

    def get_change_by_symbol(self, symbol):
        return self.get_data_by_symbol(symbol)['chg']

    def pull_data(self):
        self.data = data_scraper.pull_closing_prices()

    def write_sample(self):
        with open(self.storage + "/sample.txt", "w") as f:
            f.write("Sample output")

    def save_data(self):
        now = arrow.now("Europe/Tallinn")
        market_close = arrow.get(datetime.datetime.strptime('16:30:00', "%H:%M:%S"), "Europe/Tallinn")
        market_open = arrow.get(datetime.datetime.strptime("10:00:00", "%H:%M:%S"), "Europe/Tallinn")
        print(f"{market_open.time()} < {now.time()} < {market_close.time()}")
        trading = market_open.time() < now.time() < market_close.time()
        if trading:
            print(colored("SUSPENDED: ", "red"), "trading currently ongoing")
            return
        for stock in self.data:
            file_name = f"{self.storage}/{stock['name']}_prices.csv"
            file_exists = os.path.isfile(file_name)
            last_date = None
            yesterday = datetime.date.today() - datetime.timedelta(days=1)
            date = yesterday if now.time() < market_open.time() else datetime.date.today()
            if file_exists:
                last_date = self.get_last_date(stock['name'])
            if date == last_date:
                print(colored("SKIPPED: ", "yellow"), f"{stock['name']} (Date already registered)")
                continue
            with open(file_name, mode="a", newline="") as f:
                csv_writer = csv.DictWriter(f, ['Date', 'Close'])
                if not file_exists:
                    csv_writer.writeheader()
                print(colored("WRITING: ", "green"),
                      f"{stock['name']} (Date: {date}, Close: {self.get_closing_price_by_symbol(stock['name'])})")
                csv_writer.writerow(
                    {"Date": date, 'Close': self.get_closing_price_by_symbol(stock['name'])})
