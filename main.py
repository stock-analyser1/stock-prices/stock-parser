from data_controller import DataController
import schedule
import time
import argparse
from datetime import datetime
import arrow

controller = DataController()
parser = argparse.ArgumentParser(description='Stock parser.')
parser.add_argument("location", type=str, help="location of storage")
parser.add_argument('--schedule', action="store_true",
                    help='Schedule stock parsing when market closes')


def schedule_price_pull():
    pull_and_save()
    print("Scheduled task")
    t = datetime.strptime("16:31", "%H:%M")
    atime = arrow.get(t, "Europe/Tallinn")
    schedule.every().day.at(str(atime.time())).do(pull_and_save)
    while True:
        schedule.run_pending()
        time.sleep(1)


def pull_and_save():
    controller.pull_data()
    controller.save_data()


def control():
    parsed_args = parser.parse_args()
    controller.storage = parsed_args.location
    repeat = parsed_args.schedule
    if repeat:
        schedule_price_pull()
    else:
        pull_and_save()


if __name__ == '__main__':
    control()
