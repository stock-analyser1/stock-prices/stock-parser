import requests
from bs4 import BeautifulSoup


def pull_closing_prices():
    ret = []
    res = requests.get("https://nasdaqbaltic.com/statistics/et/shares")
    soup = BeautifulSoup(res.text, 'html.parser')
    trs = soup.select_one("table").select("tr td:nth-child(2), tr td:nth-child(3)")
    for i in range(0, len(trs), 2):
        data = trs[i: i + 2]
        ret.append({"name": data[0].text, "last": float(data[1].text.replace(",", "."))})
    return ret


if __name__ == '__main__':
    pull_closing_prices()
